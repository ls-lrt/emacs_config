;; C mode
(setq c-default-style "linux" c-basic-offset 4)
(add-hook 'c-mode-common-hook
	  (lambda()
	    (require 'dtrt-indent)
	    (dtrt-indent-mode t)))

(provide '_prog)
