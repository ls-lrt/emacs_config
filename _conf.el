(menu-bar-mode 0)
(menu-bar-mode 0)
;(tool-bar-mode 0)
(tooltip-mode 0)
;(scroll-bar-mode 0)
(blink-cursor-mode t)
(global-font-lock-mode t)
(global-hl-line-mode t)
(column-number-mode t)
(show-paren-mode t)
(ido-mode t)
(setq inhibit-startup-message t)
;(setq confirm-kill-emacs 'y-or-n-p)

(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <right>") 'windmove-right)
(global-set-key (kbd "C-x <left>") 'windmove-left)

; split window vertically
;(setq split-height-threshold nil)
;(setq split-width-threshold 0)

(require '_prog)
;(require 'magit)
(require 'rscope)

(provide '_conf)
